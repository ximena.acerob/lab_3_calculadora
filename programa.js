class Calculador{
    valor1;
    valor2;

    sumar(){
        return this.valor1 + this.valor2;
    }

    set sValor1(valor){
        this.valor1=Number(valor); //convertir texto a numero
    }

    set sValor2(valor){
        this.valor2=parseInt(valor); //convertir a numero
    }

    potenciar(){

        return Math.pow(this.valor1,this.valor2);
    }

    getBaseLogX() {
        return Math.log(this.valor2) / Math.log(this.valor1);
    }

    getBaseLogY() {
        return Math.log(this.valor1) / Math.log(this.valor2);
    }
}

class CalculadorAritmetica extends Calculador{

    sumar() {
        return this.valor1 + this.valor2;
    }

    restar() {
        return this.valor1 - this.valor2;
    }

    modular() {
        return this.valor1 / this.valor2;
    }

}

let miCalculador=new CalculadorAritmetica();

let otroCalculador=new Calculador();
